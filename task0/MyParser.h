#ifndef TASK0_B_PARSER_H
#define TASK0_B_PARSER_H

#include <string>
#include <map>
#include <algorithm>


namespace MyParser{
    std::map <std::wstring, int> parse_string(const std::wstring&);
    bool isLetter (wchar_t);
}


#endif //TASK0_B_PARSER_H
