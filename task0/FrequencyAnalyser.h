//
// Created by misha on 02.10.2020.
//

#ifndef TASK0_B_FREQUENCYANALYSER_H
#define TASK0_B_FREQUENCYANALYSER_H

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <iomanip>

namespace task0bFrequencyAnalyser {
    class FrequencyAnalyser {
    private:
        int word_number;
        std::map<std::wstring, int> table;
        std::vector<std::pair<std::wstring, int>> table_vec;

    public:
        void analyse(const std::map<std::wstring, int> &);

        void sort();

        void print_data(std::wostream *) const;
    };

}
#endif //TASK0_B_FREQUENCYANALYSER_H
