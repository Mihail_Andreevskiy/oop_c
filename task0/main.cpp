#include <fstream>
#include <clocale>
#include <string>
#include "Reader.h"
#include "MyParser.h"
#include "FrequencyAnalyser.h"

#define INPUT_FILE_DEFAULT "in.txt"
#define OUTPUT_FILE_DEFAULT "out.csv"

int main (int, char** argv) {
    setlocale(LC_ALL, "Russian"); // for russian letters
    static const int INPUT_FILE_INDEX = 1;
    static const int OUTPUT_FILE_INDEX = 2;

    task0bFrequencyAnalyser::FrequencyAnalyser Analyser;

    std::wifstream input_file;
    std::wofstream output_file;

    try {

        if (argv[INPUT_FILE_INDEX] != nullptr)
            input_file.open(argv[INPUT_FILE_INDEX]);
        else
            input_file.open(INPUT_FILE_DEFAULT);

        if (argv[OUTPUT_FILE_INDEX] != nullptr)
            output_file.open(argv[OUTPUT_FILE_INDEX]);
        else
            output_file.open(OUTPUT_FILE_DEFAULT);

    }
    catch (std::ios_base::failure &e){
        std::cout << "can't open file";
        input_file.close();
        output_file.close();
        return 0;
    }

    std::wstring str;
    Reader::read(input_file, str);

    std::map<std::wstring, int> parsed_string;
    parsed_string = MyParser::parse_string(str);

    Analyser.analyse(parsed_string);
    Analyser.sort();
    Analyser.print_data(&output_file);

    input_file.close();
    output_file.close();
    return 0;
}

