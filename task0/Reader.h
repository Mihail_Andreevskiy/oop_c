//
// Created by misha on 04.12.2020.
//

#ifndef TASK0_B_READER_H
#define TASK0_B_READER_H

#include <fstream>
#include <string>

namespace Reader {
    void read(std::wifstream&, std::wstring&);
};


#endif //TASK0_B_READER_H
