#include "MyParser.h"

namespace MyParser {

    bool isLetter(wchar_t x) {
        return ((x >= L'A' && x <= L'Z') || (x >= L'a' && x <= L'z') || (x >= L'А' && x <= L'Я') ||
                (x >= L'а' && x <= L'я') || (x >= L'0' && x <= L'9'));
    }


    std::map <std::wstring, int> parse_string (const std::wstring& string_to_parse) {
        std::map <std::wstring, int> result;
        std::wstring current_word;

        for (const auto & current_symbol : string_to_parse){
            if (isLetter(current_symbol)) {
                current_word.push_back(current_symbol);
            }
            else if(!current_word.empty()) {
                result[current_word] ++;
                current_word.clear();
            }
        }

        return result;
    }

}
