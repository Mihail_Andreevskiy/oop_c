//
// Created by misha on 02.10.2020.
//

#include "FrequencyAnalyser.h"
namespace task0bFrequencyAnalyser {
    void FrequencyAnalyser::analyse(std::map<std::wstring, int> const &map_to_analyse) {
        table = map_to_analyse;
        word_number = 0;
        for (const auto &iterator : table) {
            word_number += iterator.second;
        }
    }

    bool comp_values_descending(const std::pair<std::wstring, int> &x, const std::pair<std::wstring, int> &y) {
        return x.second > y.second;
    }

    void FrequencyAnalyser::sort() {
        for (const auto &iterator : table) {
            table_vec.emplace_back(std::make_pair(iterator.first, iterator.second));
        }
        std::sort(table_vec.begin(), table_vec.end(), comp_values_descending);

    }

    void FrequencyAnalyser::print_data(std::wostream *out) const {
        for (auto &it : table_vec) {
            *out << std::setprecision(3) << it.first << ";" << it.second << ";"
                 << (float) (it.second * 100) / (float) word_number << "%" << std::endl;
        }
    }
}