//
// Created by misha on 04.12.2020.
//

#include "Reader.h"

namespace Reader {
    void read (std::wifstream& input_file, std::wstring& result) {
        std::wstring buffer_string;
        while (getline(input_file, buffer_string)) {
            result += buffer_string;
            result.push_back(' '); // to prevent word concatenation
        }
    }
}